<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DeveloperController extends Controller
{
    /**
     * @Route("developers", name="developers")
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Developer');
        $developers = $repository->findAll();

        return $this->render('@App/developer/index.html.twig', [
            'developers' => $developers
        ]);
    }

    /**
     * @Route("developer/{id}", name="developer_item", requirements={"id": "[0-9]+"})
     */
    public function showAction($id)
    {
        $developer = $this->getDoctrine()->getRepository('AppBundle:Developer')->find($id);

        if (!$developer) {
            throw $this->createNotFoundException('No developer found for id '.$id);
        }

        return $this->render('@App/developer/products.html.twig', [
            'developer' => $developer
        ]);
    }

}
