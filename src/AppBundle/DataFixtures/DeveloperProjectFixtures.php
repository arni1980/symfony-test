<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Developer;
use AppBundle\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DeveloperProjectFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //Projects
        $this->addProjects($manager);

        //Developers
        $this->addDevelopers($manager);
    }

    /**
     * @param ObjectManager $manager
     */
    protected function addProjects(ObjectManager &$manager): void
    {
        $time = time();
        for ($i = 1; $i <= 3; $i++) {
            $project = new Project();
            $project->setName('project-'.$i);
            $randomTime = mt_rand(1, 1000000);
            $project->setCreated($time - $randomTime);
            $manager->persist($project);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    protected function addDevelopers(ObjectManager &$manager): void
    {
        for ($i = 1; $i <= 7; $i++) {
            $developer = new Developer();
            $developer->setName('Developer-'.$i);
            $randomAge = mt_rand(18, 60);
            $developer->setAge($randomAge);

            //Несколько проектов одному разработчику
            $projectNameId = ['1','2','3'];
            shuffle($projectNameId);
            for ($j = 1; $j <= 3; $j++) {
                $proj = $this->getProject($manager, array_pop($projectNameId));
                if($proj){
                    $developer->addProject($proj);
                }
            }
            $manager->persist($developer);
        }
        $manager->flush();
    }

    /**
     * Random project
     *
     * @param ObjectManager $manager
     * @param int $nameId
     * @return Project|null
     */
    private function getProject(ObjectManager $manager, int $nameId): ?Project
    {
        if (mt_rand(0, 100) < 30) return null;
        return $manager->getRepository(Project::class)->findOneBy(['name' => 'project-'.$nameId]);
    }

}